import { Categories } from "../Categories/Categories";
import { Header } from "../Header/Header";
import Products from "../Products/Products";
import { StyleMain } from "./Main.style";

import { authAxios, CategoriesType } from "../../ConstantType/ConstantType";
import { useEffect, useState } from "react";
import * as ReactBootStrap from "react-bootstrap";

interface Props {
  handleOpenLogin: (data: boolean) => void;
}

export const Main = (props: Props) => {
  const { handleOpenLogin } = props;

  const [categories, setCategories] = useState<CategoriesType[]>([]);

  const [products, setProducts] = useState([]);

  const [filterId, setFilterId] = useState(11);

  const [cart, setCart] = useState<any[]>(
    JSON.parse(localStorage.getItem("cart") || '{}')
  );

  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const res = await authAxios.get(`Master/danh-sach-loai-san-pham`);
        setCategories(res.data.data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchCategories();
  }, []);

  useEffect(() => {
    const filterFunction = async () => {
      const filterProducts = {
        filter: {
          danhmucSpId: [filterId],
          text: "",
          nguoiPhuTrach: "",
        },
        pageSize: 20,
        pageIndex: 1,
      };
      setIsLoading(true);
      try {
        const res = await authAxios.post(
          "/SanPham/search-spv1",
          filterProducts
        );
        setProducts(res.data.data.data);
      } catch (err) {
        console.log(err);
      } finally {
        setIsLoading(false);
      }
    };
    filterFunction();
  }, [filterId]);

  const handleAdd = (data: any) => {
    const cartClone = [...cart];
    const indexSelectedItem = cartClone.findIndex(
      (item) => item.id === data.id
    );
    if (indexSelectedItem !== -1) {
      cartClone[indexSelectedItem].soluong += data.soLuong;
    } else {
      cartClone.push(data);
    }
    localStorage.setItem("cart", JSON.stringify(cartClone));
    setCart(cartClone);
  };

  return (
    <StyleMain>
      <Header />
      <Categories
        categories={categories}
        filterId={filterId}
        setFilterId={setFilterId}
        cart={cart}
        handleOpenLogin={handleOpenLogin}
      />
      {!isLoading ? (
        <Products handleAdd={handleAdd} products={products} />
      ) : (
        <ReactBootStrap.Spinner animation="border" />
      )}
    </StyleMain>
  );
};
