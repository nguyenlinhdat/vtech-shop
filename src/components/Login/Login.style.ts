import styled from "@emotion/styled";

export const StyleLogin = styled.div`
  align-items: center !important;
  background-color: rgba(0, 0, 0, 0.3);
  bottom: 0;
  display: flex;
  justify-content: center !important;
  left: 0;
  position: fixed;
  right: 0;
  top: 0;
  z-index: 9999;
  & .login {
    animation: fadeIn 0.5s forwards;
    @keyframes fadeIn {
      from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }
    background-color: #fff;
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    max-width: 600px;
    min-height: 200px;
    overflow: hidden;
    padding: 8px;
    width: 70%;
  }

  & .close {
    text-align: right;
  }

  & .closeBtn {
    padding: 3px 8px;
    cursor: pointer;
    font-size: 1rem;
    font-weight: 400;
    color: #fff;
    text-align: center;
    text-decoration: none;
    vertical-align: center !important;
    background-color: #dc3545;
    border-radius: 0.375rem;
    line-height: 0.5;
    border: 1px solid #dc3545;
    user-select: none;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
      border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }

  & .title {
    animation: text 0.5s ease;
    color: #db644c;
    font-size: 20px;
    font-weight: 700;
    margin-bottom: 20px;
    text-align: center;
    transition: all 1s;
    white-space: nowrap;
  }

  & .typeLogin {
    align-items: center;
    display: flex;
    justify-content: space-around;
  }

  & .Btn {
    padding: 9px 16px;
    border: 1px solid #ccc;
    font-size: 14px;
    display: flex;
    align-items: center;
    cursor: pointer;
    border-radius: 4px;
    min-width: auto;
  }

  & .active {
    background-color: #db644c;
    color: #fff;
  }

  & .registerForm {
    display: flex;
    flex-direction: column;
    height: 400px;
    justify-content: space-between;
    padding: 10px;
  }

  & .formGroup {
    border: 1px solid #ccc;
    margin: 12px 0;
    padding: 20px;
  }

  & .form-control {
    font-size: 16px;
    padding: 8px 12px;
    display: block;
    width: 100%;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    appearance: none;
    border-radius: 0.375rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    overflow: visible;
  }

  & .submitBtn{
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #fff;
    background-color: #db644c;
    padding: 9px 16px ;
    border-radius: 4px;
    border: 1px solid #ccc;
    font-size: 16px;
    font-weight: 600;
  }

`;
