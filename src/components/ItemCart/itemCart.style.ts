import styled from "@emotion/styled";

export const StyleItemCart = styled.tr`
  & .delBtn:hover {
		color: red;
		cursor: pointer;
	}

	& .itemBtn{
		padding: 2px 8px;
		border: none;
		background-color: rgba(175,175,175,0.9);
		max-width: 20px;
	}

	& .itemBtn:hover{
		opacity: 0.8;
		cursor: pointer;
	}

	& .itemInput{
		width: 32px;
		text-align: center;
	}
`;
