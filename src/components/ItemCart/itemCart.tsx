import { StyleItemCart } from "./itemCart.style";
import DeleteIcon from "@mui/icons-material/Delete";
import { useEffect, useState } from "react";

interface Props {
  item: any;
  handleChangeQuantity : (quantity : number) => void
  handleDelItem : (id : string) => void
}

const ItemCart = (props: Props) => {
  const { item , handleChangeQuantity , handleDelItem } = props;

  const [valueInput, setValueInput] = useState<number>(1);

  useEffect(() => {
    handleChangeQuantity(valueInput)
  },[valueInput])

  const onChangeInput = (e: any) => {
    const [value] = e.target;
    setValueInput(value);
  };

  const increase = () => {
    setValueInput(valueInput + 1);
  };

  const decrease = () => {
    setValueInput(valueInput + 1);
  };

  const totalItem = item.giaBan * valueInput;

  return (
    <StyleItemCart>
      <td className="delBtn">
        <DeleteIcon 
        onClick = {() => handleDelItem(item.id)}
         />
      </td>
      <td>{item.ten}</td>
      <td>
        <button onClick={increase} className="itemBtn">
          +
        </button>
        <input
          className="itemInput"
          onChange={onChangeInput}
          value={valueInput}
        />
        <button onClick={decrease} className="itemBtn">
          -
        </button>
      </td>
      <td>{item.giaBan}</td>
      <td>{totalItem}</td>
      
    </StyleItemCart>
  );
};

export default ItemCart;
