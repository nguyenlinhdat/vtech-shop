import { StyleButton } from "../Header/Header.style";
import {
  StyleItem,
  StyleItemAction,
  StyleItemPrice,
  StyleItemSold,
  StyleItemTitle,
} from "./Item.style";

interface Props {
  imgURL: any;
  handleAdd: any;
  productsName: any;
}

export const Item = (props: Props) => {
  const { imgURL, handleAdd, productsName } = props;

  const PF = process.env.PUBLIC_FOLDER;

  const idSanPham = imgURL.map((item: any) => item.idSanPham);

  const productsArr = productsName.filter(
    (item: any) => item.id === idSanPham[0]
  );

  console.log(productsArr)

  return imgURL.map((item: any) => (
    <StyleItem className="item" key={item.id}>
      <div className="itemImg">
        <img
          src={item?.url ? item.url : PF + "anh-dep-thien-nhien-3.jpg"}
          alt="img"
        />
      </div>
      {productsArr.map((item: any ,index : number) => {
        return (
          <div key={index}>
            <StyleItemTitle>{item.ten}</StyleItemTitle>
            {/* <StyleItemSold>{}</StyleItemSold> */}
            <StyleItemPrice>
              <div className="itemPrice">{item.giaBan}$</div>
              <div className="itemDiscount">0$</div>
            </StyleItemPrice>
            <StyleItemAction>
              <div className="info">
                <StyleButton className="buttonInline">Details</StyleButton>
              </div>
              <div className="buy">
                <StyleButton
                  className="buttonInline"
                  onClick={() => handleAdd(item)}
                >
                  Add
                </StyleButton>
              </div>
            </StyleItemAction>
          </div>
        );
      })}
    </StyleItem>
  ));
};
