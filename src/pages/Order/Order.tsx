import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { json } from "stream/consumers";
import { StyledOrder } from "./Order.style";
import ItemCart  from './../../components/ItemCart/itemCart';

const Order = () => {
  const [cart, setCart] = useState<any>([]);

  const [quantity , setQuantity] = useState<number>(0)

  const handleChangeQuantity = (quantity : number) => {
    setQuantity(quantity)
  }

  useEffect(() => setCart(JSON.parse(localStorage.getItem('cart') || '{}')),[])

  const handleDelItem = (id:string) => {
    const cartCLone = [...cart];
    const newCart =  cartCLone.filter((item : any) => item.id !== id)
    localStorage.setItem('cart' , JSON.stringify(newCart))
    setCart(newCart)
  }

  const dataCart = cart.map((item : any,index : number) => {
    return <ItemCart item={item} key={index} handleChangeQuantity = {handleChangeQuantity} handleDelItem = {handleDelItem}  />
  })

  return (
    <StyledOrder className="cart">
      <header>Chi tiết đơn hàng</header>
      <div className="cartTable">
        <table className="cartTableHeader">
          <tr>
            <th colSpan={3}>Chi tiết sản phẩm</th>
            <th colSpan={2}>Giá</th>
          </tr>
          <tr>
            <th>Xóa SP</th>
            <th>Tên SP</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Tổng</th>
          </tr>
          {dataCart}
        </table>
      </div>
      <div className="cart--footer">
        <button className="cart--footer__btn">
          <span>Đặt hàng</span>
          <i></i>
        </button>
        <Link to="/">
          <button className="cart--footer__btn">
            <span>Về trang chủ</span>
            <i></i>
          </button>
        </Link>
      </div>
    </StyledOrder>
  );
};

export default Order;
