import styled from "@emotion/styled";

export const StyledOrder = styled.div`
  &.cart,
  header {
    font-size: 2.6rem;
    font-weight: bold;
    margin-top: 8px;
    margin-bottom: 32px;
  }

  & th,
  td {
    padding: 8px 0;
  }

  & .cartTable {
    margin: 0 16px;
    display: flex;
    justify-content: center;
  }

  & .cartTableHeader {
    font-size: 1.6rem;
    width: 100%;
    border-collapse: collapse;
    border-left: 2px solid #c9c4c4;
    border-right: 2px solid #c9c4c4;
    /* margin: 0 16px; */
    text-align: center;
  }

  & .cartTableHeader,
  tr,
  th,
  td {
    border: 1px solid #b7b2b2;
  }
 & .cart--footer {
    margin-top: 32px;
    display: inline-flex;
    justify-content: end;
    width: 100%;
    align-items: center;
  }
 & .cart--footer__btn {
    position: relative;
    background-color: #fff;
    color: #fff;
    text-transform: uppercase;
    font-size: 1.5rem;
    letter-spacing: 0.1rem;
    font-weight: 400;
    padding: 10px 30px;
    transition: 0.5s;
    margin: 0 8px;
  }
 & .cart--footer__btn:hover {
    letter-spacing: 0.25rem;
    color: #1e9bff;
    background-color: #1e9bff;
    box-shadow: 0 0 35px #1e9bff;
    cursor: pointer;
  }

 & .cart--footer__btn::before {
    content: "";
    position: absolute;
    inset: 2px;
    background-color: #27282c;
  }

 & .cart--footer__btn span {
    position: relative;
    z-index: 1;
  }

 & .cart--footer__btn i {
    position: absolute;
    inset: 0;
    display: block;
  }

 & .cart--footer__btn i::before {
    content: "";
    position: absolute;
    top: 0;
    left: 80%;
    width: 10px;
    height: 4px;
    background-color: #27282c;
    transform: translateX(-50%) skewX(325deg);
    transition: 0.5s;
  }

 & .cart--footer__btn:hover i::before {
    width: 20px;
    left: 20%;
  }

 & .cart--footer__btn i::after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 20%;
    width: 10px;
    height: 4px;
    background-color: #27282c;
    transform: translateX(-50%) skewX(325deg);
    transition: 0.5s;
  }

 & .cart--footer__btn:hover i::after {
    width: 20px;
    left: 80%;
  }
`;
