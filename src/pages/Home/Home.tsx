
import { Login } from '../../components/Login/Login'
import { Main } from '../../components/Main/Main'
import { Sidebar } from '../../components/Sidebar/Sidebar'
import { StyLeDefaultLayout } from './Home.style'
import { useState } from 'react';

const Home = () => {

  const [showLogin , setShowLogin] = useState<boolean>(false)

  const handleOpenLogin = (data : boolean) => {
    // setShowLogin(data)
  }

  return (
    <StyLeDefaultLayout>
      <Sidebar/>
      <Main handleOpenLogin = {handleOpenLogin}/>
      {showLogin ? <Login handleOpenLogin = {handleOpenLogin} /> : "" }
    </StyLeDefaultLayout>
  )
}

export default Home
